<?php
//$modx->setLogLevel(modX::LOG_LEVEL_INFO); // set log level
$modx->log(modX::LOG_LEVEL_ERROR,'User importer started');
$source_file = $modx->config['base_path']."assets/import/user_import.csv";
$max_line_length = 0; // maximum line length for the csv file
$header = NULL; // Initialize header
$data = array(); // Initialize data array
$today = getdate();
$createdDateTime = $today[0]; // Unix Time

// Load csv and create an associative array to allow column values to appear in any order and be found by text column key
ini_set('auto_detect_line_endings', TRUE);
if (file_exists($source_file) && ($file_handle = fopen($source_file, 'r')) !== FALSE){
	while (($data = fgetcsv($file_handle, $max_line_length, ',' )) !== FALSE){
	  if(!$header){ //get header array
	    $header = $data; // Assume the first row is the header
	    $columns = array(
	      "location_id",
	      "full_name",
	      "email_address",
	      //"extension",
	      "title");
	    foreach($columns as $column){
  	    if(!in_array($column, $header)){ //test header names
  	    	$modx->log(modX::LOG_LEVEL_ERROR,'Import Failed! There is no header item for '.$column.' in the uploaded CSV. Please reformat the CSV and try again.');
  		    fclose($file_handle);
  				unlink($source_file);
  		    return false;
		    }//end if
	    }//end foreach column
	  }else{
	    //note: if the csv is malformed, this line will explode into terribleness.
	    $row_buffer[] = array_combine($header, $data);
		}
	}
} else {
	$modx->log(modX::LOG_LEVEL_ERROR,'An error occurred opening the CSV! Please check that it is properly encoded.');
	fclose($file_handle);
  unlink($source_file);
	return false;
}//end loading csv array

foreach ($row_buffer as $row){ // test for empty rows
	if(empty($row['full_name'])){
		$modx->log(modX::LOG_LEVEL_ERROR,'Import Failed! There are one or more empty rows in the uploaded CSV. Please reformat the CSV and try again.');
		fclose($file_handle);
		unlink($source_file);
		return false; // throw up if there are any empty rows
	}
}

foreach ($row_buffer as $row){ // start import
	$modx->log(modX::LOG_LEVEL_ERROR,'Import loop started for: '.$row['full_name']);
	$user = $modx->getObject('modUser', array('username' => $row['full_name'])); // look for existing document
	if(!empty($user)){ // user exists
		$modx->log(MODX_LOG_LEVEL_ERROR, "This user already exists! (".$row['full_name'].")");
	}else{ // create new user
		$modx->log(modX::LOG_LEVEL_ERROR,'User does not exist: '.$row['full_name']);

		$new_pwd = rand();
		//$user = $modx->newObject('modUser', array ('username'=>$row['full_name']));
		$user = $modx->newObject('modUser');
		$user->set('username', $row['full_name']);
		$user->set('password',md5($new_pwd));

		$userProfile = $modx->newObject('modUserProfile');
		$userProfile->set('fullname',$row['full_name']);
		$userProfile->set('email',$row['email_address']);
		//$userProfile->set('phone',$row['extension']);
		$userProfile->set('comment',$row['title']);
		$success = $user->addOne($userProfile);
		if($success){
	    if(!$user->save()){
		    $modx->log(modX::LOG_LEVEL_ERROR, $row['full_name'].' Not Saved.');
	    }else{
		    $modx->log(modX::LOG_LEVEL_ERROR, $row['full_name'].' Saved.');
		    $user->joinGroup($row['location_id']);
	    }
		}else{
		  $modx->log(modX::LOG_LEVEL_ERROR, $row['full_name'].' Not Saved.');
		}

		if($modx->getCacheManager()){ // Clear the cache
	    $modx->cacheManager->refresh();
		}

	} // close else ($newUser)
} // close foreach loop

fclose($file_handle);
unlink($source_file);
$modx->log(modX::LOG_LEVEL_ERROR,'Import Successful!');
return true;
<?php
//$modx->setLogLevel(modX::LOG_LEVEL_INFO); // set log level
$modx->log(modX::LOG_LEVEL_ERROR,'Property importer started');
$source_file = $modx->config['base_path']."assets/import/property_import.csv";
$max_line_length = 0; // maximum line length for the csv file
$header = NULL; // Initialize header
$data = array(); // Initialize data array
$today = getdate();
$createdDateTime = $today[0]; // Unix Time

// Load csv and create an associative array to allow column values to appear in any order and be found by text column key
ini_set('auto_detect_line_endings', TRUE);
if (file_exists($source_file) && ($file_handle = fopen($source_file, 'r')) !== FALSE){
	while (($data = fgetcsv($file_handle, $max_line_length, ',' )) !== FALSE){
	  if(!$header){ //get header array
	    $header = $data; // Assume the first row is the header
	    $columns = array(
	      "property_name",
	      "yardi_code",
	      "units",
	      "street_address",
	      "city",
	      "state",
	      "zip",
	      "property_phone",
	      "property_manager",
	      "property_supervisor",
	      "property_email");
	    foreach($columns as $column){
  	    if(!in_array($column, $header)){ //test header names
  	    	$modx->log(modX::LOG_LEVEL_ERROR,'Import Failed! There is no header item for '.$column.' in the uploaded CSV. Please reformat the CSV and try again.');
  		    fclose($file_handle);
  				unlink($source_file);
  		    return false;
		    }//end if
	    }//end foreach column
	  }else{
	    //note: if the csv is malformed, this line will explode into terribleness.
	    $row_buffer[] = array_combine($header, $data);
		}
	}
} else {
	$modx->log(modX::LOG_LEVEL_ERROR,'An error occurred opening the CSV! Please check that it is properly encoded.');
	fclose($file_handle);
  unlink($source_file);
	return false;
}//end loading csv array

foreach ($row_buffer as $row){ // test for empty rows
	if(empty($row['property_name'])){
		$modx->log(modX::LOG_LEVEL_ERROR,'Import Failed! There are one or more empty rows in the uploaded CSV. Please reformat the CSV and try again.');
		fclose($file_handle);
		unlink($source_file);
		return false; // throw up if there are any empty rows
	}
}

foreach ($row_buffer as $row){ // start import
	$modx->log(modX::LOG_LEVEL_ERROR,'Import loop started for: '.$row['property_name']);
	$document = $modx->getObject('modResource', array('pagetitle' => $row['property_name'])); // look for existing document
	if(!empty($document)){ // update existing document
		$modx->log(MODX_LOG_LEVEL_ERROR, "This property already exists! (".$row['property_name'].")");
	}else{ // create new document
		$modx->log(modX::LOG_LEVEL_ERROR,'Document does not exist: '.$row['property_name']);
		$newDocument = $modx->newObject('modResource');
		$newDocument->set('type', 'document'); // Create the default fields for the document
		$newDocument->set('contentType', 'text/html');
		$newDocument->set('pagetitle', $row['property_name']);
		$newDocument->set('longtitle', '');
		$newDocument->set('description', '');
		$newDocument->set('alias', '');
		$newDocument->set('link_attributes', '');
		$newDocument->set('published', 1);
		$newDocument->set('pub_date', 1);
		$newDocument->set('unpub_date', 0);
		$newDocument->set('parent', 171);
		$newDocument->set('isfolder', 0);
		$newDocument->set('introtext', '');
		$newDocument->set('content', '');
		$newDocument->set('richtext', 1);
		$newDocument->set('template', 6);
		$newDocument->set('menuindex', 1);
		$newDocument->set('searchable', 1);
		$newDocument->set('cacheable', 1);
		$newDocument->set('createdby', $modx->user->get('id'));
		$newDocument->set('createdon',$createdDateTime);
		$newDocument->set('editedby', '');
		$newDocument->set('editedon', '');
		$newDocument->set('deleted', '');
		$newDocument->set('deletedon', '');
		$newDocument->set('deletedby', '');
		$newDocument->set('publishedon', '');
		$newDocument->set('publishedby', '');
		$newDocument->set('menutitle', '');
		$newDocument->set('hidemenu', '');
	  $newDocument->set('donthit', '');
		$newDocument->set('haskeywords', '');
		$newDocument->set('hasmetatags', '');
		$newDocument->set('privateweb', '');
		$newDocument->set('privatemgr', '');
		$newDocument->set('content_dispo', '');
		$newDocument->set('hidemenu', '');
		$newDocument->set('class_key', 'modDocument');
		$newDocument->set('context_key', 'web');
		$newDocument->set('content_type', 1);
		if (!$newDocument->save()){ // Save the document
			$modx->log(modX::LOG_LEVEL_ERROR,'An error occurred while saving '.$row['property_name']);
			fclose($file_handle);
      unlink($source_file);
	  	return false;
		}

		$modx->log(modX::LOG_LEVEL_ERROR,'document saved');

		//update TV's
		$page = $modx->getObject('modResource',array('pagetitle' => $row['property_name']));
		if (!$page->setTVValue('yardi_code', $row['yardi_code'])) {
			$modx->log(modX::LOG_LEVEL_ERROR,'There was a problem saving your TV (yardi_code).');
			fclose($file_handle);
	    unlink($source_file);
			return false;
		}
		if (!$page->setTVValue('units', $row['units'])) {
			$modx->log(modX::LOG_LEVEL_ERROR,'There was a problem saving your TV (units).');
			fclose($file_handle);
	    unlink($source_file);
			return false;
		}
		if (!$page->setTVValue('street_address', $row[street_address])) {
			$modx->log(modX::LOG_LEVEL_ERROR,'There was a problem saving your TV (street_address).');
			fclose($file_handle);
	    unlink($source_file);
			return false;
		}
		if (!$page->setTVValue('city', $row['city'])) {
			$modx->log(modX::LOG_LEVEL_ERROR,'There was a problem saving your TV (city).');
			fclose($file_handle);
	    unlink($source_file);
			return false;
		}
		if (!$page->setTVValue('state', $row['state'])) {
			$modx->log(modX::LOG_LEVEL_ERROR,'There was a problem saving your TV (state).');
			fclose($file_handle);
	    unlink($source_file);
			return false;
		}
		if (!$page->setTVValue('zip', $row['zip'])) {
			$modx->log(modX::LOG_LEVEL_ERROR,'There was a problem saving your TV (zip).');
			fclose($file_handle);
	    unlink($source_file);
			return false;
		}
		if (!$page->setTVValue('property_phone', $row['property_phone'])) {
			$modx->log(modX::LOG_LEVEL_ERROR,'There was a problem saving your TV (property_phone).');
			fclose($file_handle);
	    unlink($source_file);
			return false;
		}
		if (!$page->setTVValue('property_manager', $row['property_manager'])) {
			$modx->log(modX::LOG_LEVEL_ERROR,'There was a problem saving your TV (property_manager).');
			fclose($file_handle);
	    unlink($source_file);
			return false;
		}
		if (!$page->setTVValue('property_supervisor', $row['property_supervisor'])) {
			$modx->log(modX::LOG_LEVEL_ERROR,'There was a problem saving your TV (property_supervisor).');
			fclose($file_handle);
	    unlink($source_file);
			return false;
		}
		if (!$page->setTVValue('property_email', $row['property_email'])) {
			$modx->log(modX::LOG_LEVEL_ERROR,'There was a problem saving your TV (property_email).');
			fclose($file_handle);
	    unlink($source_file);
			return false;
		}
		if($modx->getCacheManager()){ // Clear the cache
	    $modx->cacheManager->refresh();
		}

		//create user group
		$yardi_code = $row['yardi_code'];
		$page_id = $page->get('id');
		$newUserGroup = $modx->newObject('modUserGroup',array('name' => $yardi_code, 'description'=>$page_id));
		if(!$newUserGroup->save()){
			$modx->log(modX::LOG_LEVEL_ERROR,'Unable to save the User Group. '.$yardi_code.' The Group may already exist.');
			fclose($file_handle);
      unlink($source_file);
	  	return false;
		}
	} // close else ($newDocument)
} // close foreach loop

fclose($file_handle);
unlink($source_file);
$modx->log(modX::LOG_LEVEL_ERROR,'Import Successful!');
return true;
$(document).ready(function() {

  //Media Queries-----------------------------------------------
	var queries = [
	  {
	    context: 'range_0',
	    match: function() {
	      //responsiveEqualHeight();
	      slideDrawerEnable();
        document.documentElement.id = 'range_0';
        console.log('current range:', MQ.new_context);
	    },
	    unmatch: function() {
	    	slideDrawerDisable();
	    }
	  },
	  {
	    context: 'range_1',
	    match: function() {
	      slideDrawerEnable();
        document.documentElement.id = 'range_1';
        console.log('current range:', MQ.new_context);
	    },unmatch: function() {
	      slideDrawerDisable();
	    }
	  },
	  {
	    context: 'range_2',
	    match: function() {
	      tocWidth();
	      slideDrawerEnable();
        document.documentElement.id = 'range_2';
        console.log('current range:', MQ.new_context);
	    },unmatch: function() {
	      slideDrawerDisable();
	      tocWidth();
	    }
	  },
	  {
	    context: 'range_3',
	    match: function() {
	      slideDrawerDisable();
	      tocWidth();
        document.documentElement.id = 'range_3';
        console.log('current range:', MQ.new_context);
	    },unmatch: function(){
  	    tocWidth();
	    }
	  },
	  {
	    context: 'range_4',
	    match: function() {
	      slideDrawerDisable();
	      tocWidth();
        document.documentElement.id = 'range_4';
        console.log('current range:', MQ.new_context);
	    },unmatch: function(){
	    	tocWidth();
	    }
	  },
	  {
	    context: 'range_5',
	    match: function() {
	      slideDrawerDisable();
	      tocWidth();
        document.documentElement.id = 'range_5';
        console.log('current range:', MQ.new_context);
	    }
	  }
	];
	MQ.init(queries);
	var range = MQ.new_context;

	$('a.mobile_nav_button').click(function(e){
		e.preventDefault();
		$('#general nav').toggle();
	});

	$('#search').hover(function(){
		var originalVal = $(this).find('input[type="text"]').val();
		if(originalVal === "SEARCH"){
			$('#search input[type="text"]').val('');
		}
	},function(){
		var newVal = $('#search input[type="text"]').val();
		if(newVal == ""){
			$('#search input[type="text"]').val("SEARCH");
		}
	});

	$('#general nav .weblink, .general_button.weblink, #altnav .weblink ').append('<span class="glyphicon glyphicon-share" aria-hidden="true"></span>');
	//$('.general_button.weblink').append('<span class="glyphicon glyphicon-share" aria-hidden="true"></span>');

	if($('body.page-11').length){
		$('#altnav').wrapInner( "<div class='row'></div>");
		$('#altnav a').wrap("<div class='col-lg-6 col-md-6 col-ms-6'></div>");
	}

	if($('form').length){
		$('.supplemental').hide();
		$('.trigger').change(function(){
      var value = $(this).val();
      console.log(value);
      if(value == '' || value == ' '){
        $('.supplemental').hide().find('input').removeClass('required');
        console.log("test off");
      }else{
        $('.supplemental').show().find('input').addClass('required');
        console.log("test on");
      }
    });
	}

	if($('body.type-2').length){
    $('.feed .slider').cycle({
      speed: 2000,
      timeout: 5500,
      fx: 'fade',
      width: '100%',
      fit: 1,
      slides: '> li'
    });
		$('.feed .slider').on( 'cycle-before', function(event, optionHash, outgoingSlideEl, incomingSlideEl, forwardFlag) {
    	$('.feed-bg').stop(true, true).animate({opacity: 0.5}, 1450);
		});
		$('.feed .slider').on( 'cycle-after', function(event, optionHash, outgoingSlideEl, incomingSlideEl, forwardFlag) {
    	$('.feed-bg').stop(true, true).animate({opacity: 0}, 1000);
		});
	}
	
	if($('body.page-32').length){
    $('#general nav a[title="Employee Manuals"]').parent().addClass('active'); 
	}


	function tocWidth(){
		if($('body.parent-4').length){
			$('.toc').columnizeList({columnAmount:2});
		}
	}

	function slideDrawerEnable(){
    var range = MQ.new_context;
    var trigger = '.snap-trigger-';
    var content = '#content_container';
    var drawer = '.snap-drawer-';
    //var contentBorder = '#content_push';
    $('.snap-drawers').show();
    $(content).addClass('active');
    $(trigger+'left').show();
    $(trigger+'right').show();
    $('#content_container.active').unbind();
    $(trigger+'left').click(function(){
      if($(content+'.open-left').length){
        $(content).stop().animate({marginLeft: '0px'}, function(){
          $(this).removeClass('open-left');
          $(drawer+'left').hide();
              //$(contentBorder).removeClass('open-left');
           $('#content_container.active').unbind();
        });

      }else{
        $(content).addClass('open-left').stop().animate({marginLeft: '200px'}, function(){
           $('#content_container.active').click(function(){
            //alert("click");
            $(content).stop().animate({marginLeft: '0px'}, 200, function(){
              $(this).removeClass('open-left');
              //$(contentBorder).removeClass('open-left');
              $(drawer+'left').hide();
              $('#content_container.active').unbind();
            });//animte close
          });//click
          $(window).on('scroll', function() {
             $(content).stop().animate({marginLeft: '0px'}, 200, function(){
              $(this).removeClass('open-left');
              //$(contentBorder).removeClass('open-left');
              $(drawer+'left').hide();
              $('#content_container.active').unbind();
            });//animte close
          });
        });//animate
        //$(contentBorder).addClass('open-left');
        $(drawer+'left').show();
      }
    });
    $(trigger+'right').click(function(){
      if($(content+'.open-left').length){
        $(content).stop().animate({marginLeft: '0px'}, function(){
          $(this).removeClass('open-left');
          $(drawer+'left').hide();
          //$(contentBorder).removeClass('open-left');
           $('#content_container.active').unbind();
        });
      }
      if($(content+'.open-right').length){
        $(content).stop().animate({marginLeft: '0px'}, function(){
          $(this).removeClass('open-right');
          $(drawer+'left').hide();
              //$(contentBorder).removeClass('open-right');
           $('#content_container.active').unbind();
        });
      }else{
        $(content).addClass('open-right').stop().animate({marginLeft: '-200px'}, function(){
           $(drawer+'left').hide();
           $('#content_container.active').click(function(){
            $(content).stop().animate({marginLeft: '0px'}, 200, function(){
              $(this).removeClass('open-right');
              //$(contentBorder).removeClass('open-right');
              $(drawer+'right').hide();
              $('#content_container.active').unbind();
            });//animte close
          });//click
          $(window).on('scroll', function() {
             $(content).stop().animate({marginLeft: '0px'}, 200, function(){
              $(this).removeClass('open-right');
              //$(contentBorder).removeClass('open-right');
              $(drawer+'right').hide();
              $('#content_container.active').unbind();
            });//animte close
          });
        });//animate
        //$(contentBorder).addClass('open-right');
        $(drawer+'right').show();
      }
    });
  }//snapDrawerEnable

  function slideDrawerDisable(){
    var trigger = '.snap-trigger-';
    var content = '#content_container';
    //var contentBorder = '#content_push';
    $('.snap-drawers').hide();
    $(content).removeClass('active');
    $('#content_container.active').unbind();
  	$(trigger+'left').unbind().hide();
    $(trigger+'right').unbind().hide();
    if($(content+'.open-left').length){
      $(content).stop().animate({marginLeft: '0px'}, function(){
        $(this).removeClass('open-left');
        //$(contentBorder).removeClass('open-left');
      });
    }
    if($(content+'.open-right').length){
      $(content).stop().animate({marginLeft: '0px'}, function(){
        $(this).removeClass('open-right');
        //$(contentBorder).removeClass('open-right');
      });
    }
  }


	//Data tables
	$('#data-table').DataTable();
	$('#data-table-emp').DataTable();

	//Backstretch
	$('.page-174,.page-175,.page-177,.page-178').backstretch('assets/img/elements/gaia-home.png', {centeredY: false});


}); //End Document Ready